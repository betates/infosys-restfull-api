package com.infosys.restfullapi.controller;

import com.infosys.restfullapi.model.Employee;
import com.infosys.restfullapi.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/employee")
public class EmployeeController {
    @Autowired
    EmployeeService employeeService;

    @GetMapping("/findall")
    public List<Employee> findAll() {
        return employeeService.findAll();
    }

    @GetMapping("/{id}")
    public Employee findById(@PathVariable(value = "id") Long id) {
        return employeeService.findByNim(id);
    }

    @PostMapping(value = "/insert", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public String insert(@RequestBody Employee employee) {
        return employeeService.insert(employee);
    }

    @PutMapping("/update/{id}")
    public String update(@PathVariable(value = "id") Long id, @RequestBody Employee employee) {
        return employeeService.update(id, employee);
    }

    @DeleteMapping("/delete/{id}")
    public String delete(@PathVariable(value = "id") Long id) {
        return employeeService.deleteById(id);
    }

}
