package com.infosys.restfullapi.service;

import com.infosys.restfullapi.model.Employee;
import com.infosys.restfullapi.repository.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EmployeeService{
    @Autowired
    EmployeeRepository employeeRepository;

    public List<Employee> findAll() {
        return employeeRepository.findAll();
    }

    public Employee findByNim (Long id) {
        return employeeRepository.findByNim(id);
    }

    public String insert(Employee employee) {
        employeeRepository.save(employee);
        return "Succesfully create User Employee";
    }

    public String update(Long id, Employee employee) {
        Employee resultEmployee = employeeRepository.findByNim(id);
        if (resultEmployee == null) {
            return "Employee ID " + id + " Not Found!";
        }
        if (!employee.getName().isEmpty()) {
            resultEmployee.setName(employee.getName());
        }
        if (!employee.getAddress().isEmpty()) {
            resultEmployee.setAddress(employee.getAddress());
        }
        if (employee.getSalary() != 0) {
            resultEmployee.setSalary(employee.getSalary());
        }
        if (employee.getPhoneNumber().isEmpty()) {
            resultEmployee.setPhoneNumber(employee.getPhoneNumber());
        }
        employeeRepository.save(resultEmployee);
        return "Successfully update User Employee with ID " + id;
    }

    public String deleteById(Long id) {
        Employee employee = employeeRepository.findByNim(id);
        if (employee == null) {
            return "User Employee with ID " + id + " Not Found!";
        }
        employeeRepository.deleteById(id);

        return "Successfully delete User Employee with ID " + id;
    }
}
