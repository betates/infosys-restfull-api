package com.infosys.restfullapi.employeetest;

import com.infosys.restfullapi.model.Employee;
import com.infosys.restfullapi.repository.EmployeeRepository;
import com.infosys.restfullapi.service.EmployeeService;
import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.times;

@RunWith(SpringRunner.class)
@SpringBootTest
class EmployeeServiceTest {

    @Autowired
    private EmployeeService employeeService;

    @MockBean
    @Autowired
    private EmployeeRepository employeeRepository;

    @Before
    public void init() {
    }

    @Test
    void findAllTestSuccess() {
        // given
        // when
        employeeService.findAll();
        // then
        Mockito.verify(employeeRepository, times(1)).findAll();
    }

    @Test
    void findByNimTestSuccess() {
        // given
        // when
        employeeService.findByNim(anyLong());
        // then
        Mockito.verify(employeeRepository, times(1)).findByNim(anyLong());
    }

    @Test
    void insertEmployeeTestSuccess(){
        // given
        // when
        Employee employee = new Employee(
                "Nama",
                "Alamat",
                0,
                "Nomor Telepon"
        );
        employeeService.insert(employee);
        // then
        Mockito.verify(employeeRepository, times(1)).save(any());
    }

    @Test
    void updateEmployeeTestSuccess(){
        // given
        final Employee employee = new Employee(1L, "Nama", "Alamat", 0, "Nomor Telepon");
        Mockito.when(employeeRepository.findByNim(1L)).thenReturn(employee);
        // when
        Employee resultEmployee = new Employee("Beta", "Alamat Beta", 0, "Nomor Beta");
        employeeService.update(2L, resultEmployee);
        // then
        Mockito.verify(employeeRepository, times(1)).save(any());
    }

//    @Test
//    void updateEmployeeTestFail(){
//        // given
//        Mockito.when(employeeRepository.findByNim(1L)).thenReturn(null);
//        // when
//        // then
//        assertThat(employeeService.update(1L, any()))
//                .isEqualTo("Employee with ID 1 Not Found!");
//    }

    @Test
    void deleteEmployeeTestSuccess(){
        // given
        final Employee employee = new Employee(1L, "Nama", "Alamat", 0, "Nomor Telepon");
        Mockito.when(employeeRepository.findByNim(1L)).thenReturn(employee);
        // when
        employeeService.deleteById(employee.getId());
        //then
        Mockito.verify(employeeRepository, times(1)).deleteById(2L);
    }

//    @Test
//    void deleteEmployeeTestFail(){
//        // given
//        Mockito.when(employeeRepository.findByNim(anyLong())).thenReturn(null);
//        // when
//        //then
//        assertThat(employeeService.deleteById(1L))
//                .isEqualTo("Employee with ID 1 Not Found!");
//    }


}