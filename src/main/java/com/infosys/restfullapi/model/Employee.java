package com.infosys.restfullapi.model;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "employee")
public class Employee {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", length = 100)
    private Long id;

    @Column(name = "name")
    private String name;
    @Column(name = "address")
    private String address;
    @Column(name = "salary")
    private Integer salary;
    @Column(name = "phoneNumber")
    private String phoneNumber;

    public Employee(){
    }

    public Employee(String name, String address, Integer salary, String phoneNumber){
        this.name = name;
        this.address = address;
        this.salary = salary;
        this.phoneNumber = phoneNumber;
    }

    public Employee(Long id, String name, String address, Integer salary, String phoneNumber){
        this.id = id;
        this.name = name;
        this.address = address;
        this.salary = salary;
        this.phoneNumber = phoneNumber;
    }

}
